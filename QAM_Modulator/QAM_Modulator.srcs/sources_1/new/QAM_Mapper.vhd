----------------------------------------------------------------------------------
-- Company: Tyndall National Institute 
-- Engineer: Mustafa Sultan
-- 
-- Create Date: 06.06.2024 14:41:30
-- Design Name: QAM4 Mapper
-- Module Name: QAM4_Mapper - Behavioral
-- Project Name: QAM Modulation
-- Target Devices: Nexys 4
-- Tool Versions: Vivado 2019.1
-- Description: 4-QAM Mapper constellation mapper
-- 
-- Revision:
-- Revision 0.02 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity QAM_Mapper is 
    Port (
        inputData       : in std_logic_vector (1 downto 0);
        iSymbols        : out std_logic_vector (1 downto 0);    -- In-Phase Symbols
        qSymbols        : out std_logic_vector (1 downto 0);    -- Quadrature-Phase Symbols
        clk             : in std_logic;                        -- System Clock
        rst             : in std_logic                          -- Asynch. Active-high reset
    );
end QAM_Mapper;

architecture Behavioral of QAM_Mapper is
    -- Constellation Size
    constant constellationSize : integer := 4;
    -- Type for the constellation
    type constellation is array (0 to (constellationSize - 1)) of signed (1 downto 0);
    
    -- 4-QAM Constellation
    -- Bits In  | Output Symbol
    --             Re  Im
    -- (00)     | (+1, +1)
    -- (01)     | (-1, +1)
    -- (10)     | (+1, -1)
    -- (11)     | (-1, -1)

    -- Real part of the 4-QAM constellation (I-array)
    constant QAM4ConstRe : constellation := (
        to_signed(1,  2),
        to_signed(-1, 2),
        to_signed(1,  2),
        to_signed(-1, 2)
    );

    -- Imaginary part of the 4-QAM constellation (Q-Array)
    constant QAM4ConstIm : constellation := (
        to_signed(1, 2),
        to_signed(1,  2),
        to_signed(-1, 2),
        to_signed(-1, 2)
    );

    -- Signal 
    signal unsignedInputData : unsigned (1 downto 0);

begin
    -- Use the input bits to index the constellation arrays
    unsignedInputData <= unsigned(inputData);
    
    qamMapper_p: process(clk, rst)
    begin
        if rst = '1' then
            iSymbols <= (others => '0');
            qSymbols <= (others => '0');
        elsif rising_edge(clk) then
            -- Map the input data bits to QAM symbols
            iSymbols <= std_logic_vector(QAM4ConstRe(to_integer(unsignedInputData)));
            qSymbols <= std_logic_vector(QAM4ConstIm(to_integer(unsignedInputData)));
        end if;
    end process qamMapper_p;
end Behavioral;
